require "colorize"
require_relative 'ship'
require_relative 'asteroid'

class Board
    attr_accessor :row, :column, :board, :position, :validate #, #:array_ast # se puede dar accedo a los atributos

    def initialize( row:, column:)
        @row = row
        @column = column
        @board = Array.new(@row){Array.new(@column)}
        @nave = Ship.new()
        @position = @column/2
        @validate = false
#        @array_ast = [] #Hay que mejorar
#        @contador = 1 #Hay que mejorar
    end

    def insert_ship
      @board.last[@position] = @nave.spacex
    end

    def paint_board

        puts '  ▄████████    ▄███████▄    ▄████████  ▄████████    ▄████████      ▀████    ▐████▀   '.colorize(:blue)
        puts ' ███    ███   ███    ███   ███    ███  ███    ███   ███    ███        ███▌   ████▀      '
        puts ' ███    █▀    ███    ███   ███    ███  ███    █▀    ███    █▀          ███  ▐███        '.colorize(:blue)
        puts ' ███          ███    ███   ███    ███  ███         ▄███▄▄▄             ▀███▄███▀        '
        puts ' ▀███████████ ▀█████████   ██████████  ███         ▀███▀▀▀             ████▀██▄       '.colorize(:blue)
        puts '          ███  ███         ███    ███  ███    █▄    ███    █▄         ▐███  ▀███          '
        puts '   ▄█    ███   ███         ███    ███  ███    ███   ███    ███       ▄███     ███▄     '.colorize(:blue)
        puts '▄████████▀   ▄████▀        ███    █▀   ████████▀    ██████████      ████       ███▄     '
                                                                                                       
        puts
        puts "     ¡Bicho asteroides se acercan ponte pilas!. ".colorize(:red)
        puts
        print "▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄".colorize(:blue)
        print "\n"
        @board.each do |row| 
            print '█'.colorize(:blue)
            row.each do |column|
               if column == "╣█╠"
                print "╣█╠".colorize(:color => :black, :background => :white)
               elsif column == "X"
                print "   ".colorize(:color => :black, :background => :blue )
               elsif column == "Y"
                print "   ".colorize(:color => :black, :background => :black )
               elsif column == "O"
                print "   ".colorize(:color => :black, :background => :yellow )
               else    
                print "   ".colorize(:color => :black, :background => :white)
               end
            end
            print '█'.colorize(:blue)  
            puts
        end
        print "▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄".colorize(:blue)
        print "\n"
        print 'Movimientos: ( a ) Izquierda y ( d ) Derecha'
        puts
    end

    def validate_1
        a = 0
        @board.last.each do |column|
            if column == "╣█╠"
                a += 1
            end
            if a == 1
                @validate = false
            else
                @validate = true
            end
            @validate
        end
    end

    def ship_move(key)
            if key == 'a'    
                @board.last[@position - 1] = "╣█╠"
                @board.last[@position] = ""
                @position -= 1
            end

            if key == 'd'
                @board.last[@position + 1] = "╣█╠"
                @board.last[@position] = ""
                @position += 1  
            end
    end

    def asteroids_move (array_ast)                  #movimiento
        array_ast.each { |element|  
            if element.size == 2 
                if element.x < @row
                    @board[element.x][element.y] = 'X'#.colorize(:yellow)
                    @board[element.x][element.y + 1] = 'X'#.colorize(:yellow)
                end
            #elsif element.size == 2 && element.x < @row && element.x >= 2
                if element.x >= 2 && element.x < @row + 2
                   @board[element.x - 2][element.y] = ' '
                   @board[element.x - 2][element.y + 1] = ' '
                end
            elsif element.size == 3 
                if element.x < @row
                    @board[element.x][element.y] = 'O'#.colorize(:blue)
                    @board[element.x][element.y + 1] = 'O'#.colorize(:blue)
                    @board[element.x][element.y + 2] = 'O'#.colorize(:blue)
                end
                if element.x >= 3 && element.x < @row + 3
                    @board[element.x - 3][element.y] = ' '
                    @board[element.x - 3][element.y + 1] = ' '
                    @board[element.x - 3][element.y + 2] = ' '
                end
            elsif element.size == 4 
                if element.x < @row
                    @board[element.x][element.y] = 'Y'#.colorize(:red)
                    @board[element.x][element.y + 1] = 'Y'#.colorize(:red)
                    @board[element.x][element.y + 2] = 'Y'#.colorize(:red)
                    @board[element.x][element.y + 3] = 'Y'#.colorize(:red)
                end
                if element.x >= 4 && element.x < @row + 4
                    @board[element.x - 4][element.y] = ' '
                    @board[element.x - 4][element.y + 1] = ' '
                    @board[element.x - 4][element.y + 2] = ' '
                    @board[element.x - 4][element.y + 3] = ' '
                end
            end
        }
        #sleep 1.0
        #system('clear')
    end

end
#ayuda para hacer los test
#@tablero = Board.new(row: 20, column: 15)
#@tablero.insert_ship
#@tablero.paint_board   
#@tablero.ship_move('a')
#@asteroid =
#@tablero.paint_board   
#@tablero.array_asteroid
#puts @tablero.array_ast[0].x
#uts @tablero.array_ast[1].x
#puts @tablero.array_ast[2].x
#puts @tablero.array_ast[3].x
#@tablero.asteroids_move
#@tablero.paint_board  
