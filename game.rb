require_relative 'utils'
require_relative 'board'
require_relative 'asteroid'

class Game
  
  def self.start
    game = Game.new 
    game.update
  end

  def initialize
    @tablero = Board.new(row: 15, column: 20)
    @tablero.insert_ship
    #@asteroid =
    @tablero.paint_board     
    @frames = 0
    @fps = 12
    @array_ast = []# Hay que mejorarlo
    #@array_ast_2 = [] # PRUEBAAAAAAAAAAAAAAa
    @score = 0
    @frames_2 = 0
    @frames_3 = 0
    @aux = 0
  end

  def update
    loop do
      @frames += 1
      @frames_2 += 1 if @frames % 2 == 0
      @frames_3 += 1 if @frames % 3 == 0
      handle_input
      @array_ast << Asteroid.new(@frames) if @frames % 4 == 0
      if rand(2) == 1 && @frames % 4 == 0
        loop do
          @aux = Asteroid.new(@frames)
          break if @aux.range[1]<@array_ast.last.range[0] || @aux.range[0] > @array_ast.last.range[1]
        end
        @array_ast << @aux
      end
      #@array_ast_2 << Asteroid.new(@frames_3) if @frames_3 % 4 == 0
      #@array_ast << Asteroid.new(@frames) if @frames % 4 == 0 # MEJORAR. Falta validacion
      @array_ast.each { |element|  element.x =  (@frames - element.origin)}  #Define la velocidad4
      #@array_ast_2.each { |element|  element.x =  (@frames_3 - element.origin)} 
      @tablero.asteroids_move(@array_ast)
      #@tablero.asteroids_move(@array_ast_2)
      @tablero.validate_1
      score
      game_over if @tablero.validate
      system("clear")
      @tablero.paint_board
      puts "Tu puntuacion es #{@score}"
      if @frames < 100
        sleep (2.4 / @fps)
      elsif @frames < 200
        sleep (1.0/@fps)
      else
        sleep (0.4/@fps)
      end

      you_win if @frames == 250
      
    end
  end

  def score
    @array_ast.each { |element|
    if element.x > @tablero.row + element.size &&element.live == true
        @score += element.points
        element.live = false
    end
  }
  end


  def handle_input
    key = Utils.get_key&.chr
    case key
    when 'a'
      if @tablero.position > 0
        @tablero.ship_move('a')
      end
    when 'd'
      if @tablero.position < 14
        @tablero.ship_move('d')
      end
    when 'x'
      game_over
    end
  end

  def game_over
    system 'clear'
    puts "------------GAME OVER--------------".colorize(:color => :red, :background => :white)
    puts @score
    sleep 2
    raise StopIteration
  end

  def you_win
    system 'clear'
    puts "                                         ".colorize( :background => :green)
    puts "               ¡GANADOR!                 ".colorize( :background => :green)
    puts "                                         ".colorize( :background => :green)
    puts @score
    sleep 2
    raise StopIteration
  end
end
