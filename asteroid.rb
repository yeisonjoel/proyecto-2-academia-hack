require "colorize"

class Asteroid
    attr_accessor :x, :y, :points, :live
    attr_reader :origin, :size, :range


    def initialize(frames)         
        @size = size_ast
        @x = 0
        @y = coord_ast_y
        @origin = frames
        @points = point
        #@color =
        #@move =
        #@speed =
        @live = true
        @range = range_init

    end

    def size_ast
        @size = rand(3) + 2
    end

    def coord_ast_y
        @y = rand(20-@size)
    end

    def point
        if @size == 2
            @points = 10
        elsif @size == 3
            @points = 20
        else
            @points = 30
        end
    end

    def range_init
        if @size == 2
            @range_init = [@y, @y+1]
        elsif @size == 3
            @range_init = [@y, @y +2]
        else
            @range_init = [@y, @y + 3]
        end
    end


end